��    M      �              �     �  	   	            	   )     3     9     G     K     d     k     t     �     �     �     �     �     �     �     �     �               1     K     Y     `     e     t     y          �     �     �     �     �      �       
             3     ?     B     S  
   \     g     |     �     �     �     �     �     �     �     �     �     �               (     /     ;     J     R     _     m     v     �     �     �     �     �     �  	   �     �     �  �  �     �
  
   �
  	     	     	             #     /     4     N  
   V     a     o     �     �     �  
   �     �     �  	   �     �     �     �          /     A     G     M     [     a     g     z     �     �     �     �  "   �     �  
   �     	          $     '     ?     L     R     g     z     �     �     �     �     �     �     �     �     �     �               &     /     ;     D     U     e     q     �     �     �     �     �     �  	   �     �     �   %(country_name)s is part of (century) (decade) (millennium) 404 error About About Paulina Age Applies to %(countries)s Author Author/s Carlos Gardel Commons link Copyright status Countries and terms Country of birth Country of origin Date of birth Date of death Description Discover authors Documentation Edit Wikidata item Flag of %(country_name)s. Free software Gender Help Help translate Home Image Image of %(name)s. Image of %(title)s. Kiyohara Yukinobu Language List of %(term_name)s List of works List of works by %(author_name)s Mary Shelley Metropolis Missing information? Nationality No No results found No value Occupation Orlando: A Biography Page not found Pampa Paulina Paulina Luisi Paulina logo Place of birth Powered by Wikidata. Publication date Results Search Search results Search with Paulina Search works Select Source code The Araucaniad Unknown Unknown term Unknown value Wikidata Wikimedia Commons Wikimedistas de Uruguay Wikipedia link Wikiquote link Wikisource link Work Yes home page in project's code repository Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2025-01-24 19:42-0300
PO-Revision-Date: 2024-09-14 22:09+0000
Last-Translator: phlostically <phlostically@mailinator.com>
Language: eo
Language-Team: Esperanto <https://hosted.weblate.org/projects/paulina/paulina-app/eo/>
Plural-Forms: nplurals=2; plural=n != 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.16.0
 %(country_name)s estas parto de (jarcento) (jardeko) (jarmilo) Eraro 404 Pri Pri Paulina Aĝo Efektiva en %(countries)s Aŭtoro Aŭtoro(j) Carlos Gardel Komuneja hiperligo Kopirajta stato Landoj kaj terminoj Naskiĝlando Devenlando Naskiĝdato Mortdato Priskribo Malkovri aŭtorojn Dokumentaro Modifi Vikidatuman eron Flago de %(country_name)s. Libera programaro Sekso Helpo Helpi traduki Hejmo Bildo Bildo de %(name)s. Bildo de %(title)s. Kijohara Jukinobu Lingvo Listo de %(term_name)s Listo de verkoj Listo de verkoj de %(author_name)s Mary Shelley Metropolis Ĉu informoj mankas? Lando Ne Neniu rezulto troviĝis Neniu valoro Okupo Orlando: A Biography Paĝo ne troviĝis Pampa Paulina Paulina Luisi Emblemo de Paulina Naskiĝloko Funkciante per Vikidatumoj. Dato de eldono Rezultoj Serĉi Serĉrezultoj Serĉi per Paulina Serĉi verkoj Elekti Fontkodo La Araucana Nekonata Nekonata termino Nekonata valoro Vikidatumoj Vikimedia Komunejo Wikimedistas de Uruguay Vikipedia hiperligo Vikicitara hiperligo Vikifontara hiperligo Verko Jes ĉefpaĝo en projekta kododeponejo 