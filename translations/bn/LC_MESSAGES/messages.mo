��    D      <              \  !   ]  	        �     �  	   �     �     �     �     �     �     �     �     �          $  A   6  ;   x     �     �     �     �     �               #     =     D     I     N     T     g     {     �     �     �     �     �     �     �                    4  
   =     H     W     _     m     z     �     �     �     �     �     �     �     �     �     �               *     9     I     N  	   R      \  �  }  :   J
     �
     �
     �
     �
     �
  .   �
  8   ,  	   e     o  ;   |     �  (   �        "   "  �   E  �     A   �  "   �       !   *     L  	   _  T   i  '   �     �     �          ,     <      \  9   }  4   �     �  N   �  '   H  %   p     �     �     �  9   �  b        j     ~  <   �     �  %   �  "        '  +   C     o  (     8   �  5   �          '     D     ^  .   }  D   �  .   �  +      +   L     x     �     �  Y   �   %(country_name)s's copyright term (century) (decade) (millennium) 404 error About About Paulina Add it to Wikidata Age Author Available for download Commons link Copyright status Countries and terms Country of origin Creative Commons Attribution-ShareAlike 4.0 International License Creative Commons CC0 1.0 Universal Public Domain Dedication Creative Commons Uruguay Data Uruguay Date of birth Date of death Description Documentation Edit Wikidata item Flag of %(country_name)s. Gender Help Home Image Image of %(name)s. Image of %(title)s. In public domain? Kiyohara Yukinobu Language Learn about the public domain List of %(term_name)s List of works Mary Shelley Nationality No No results found No results found. Try again. No value Occupation Page not found Paulina Paulina Luisi Paulina logo Place of birth Publication date Results Search Search results Search works Unknown Unknown term Unknown value Wikidata Wikimedia Commons Wikimedistas de Uruguay Wikipedia link Wikiquote link Wikisource link Work Yes home page Ártica - Online Cultural Center Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2025-01-24 19:42-0300
PO-Revision-Date: 2024-09-04 19:48+0000
Last-Translator: Jorge Gemetto <jorgemet@gmail.com>
Language: bn
Language-Team: Bengali <https://hosted.weblate.org/projects/paulina/paulina-app/bn/>
Plural-Forms: nplurals=2; plural=n > 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.16.0
 %(country_name)s-এর কপিরাইট শর্ত (শতাব্দী) (দশক) (সহস্রাব্দ) ৪০৪ ত্রুটি সম্পর্কে পাউলিনা সম্পর্কে উইকিউপাত্তে যোগ করুন বয়স লেখক ডাউনলোডের জন্য উপলব্ধ কমন্স লিঙ্ক কপিরাইট অবস্থা দেশ এবং শর্ত উৎপত্তির দেশ ক্রিয়েটিভ কমন্স অ্যাট্রিবিউশন-শেয়ারঅ্যালাইক ৪.০ আন্তর্জাতিক লাইসেন্স ক্রিয়েটিভ কমন্স সিসি০ ১.০ ইউনিভার্সাল পাবলিক ডোমেইন ডেডিকেশন ক্রিয়েটিভ কমন্স উরুগুয়ে ডাটা উরুগুয়ে জন্মতারিখ মৃত্যুতারিখ বর্ণনা নথি উইকিউপাত্ত আইটেম সম্পাদনা করুন %(country_name)s-এর পতাকা লিঙ্গ সাহায্য প্রধান পাতা চিত্র %(name)s-এর চিত্র %(title)s-এর চিত্র পাবলিক ডোমেইনে রয়েছে? কিয়োহারা ইয়ুকিনোবু ভাষা পাবলিক ডোমেইন সম্বন্ধে জানুন %(term_name)s-এর তালিকা কর্মের তালিকা মেরি শেলি জাতীয়তা না কোন ফলাফল পাওয়া যায়নি ফলাফল পাওয়া যায়নি। আবার চেষ্টা করুন। মান নেই পেশা পাতা খুঁজে পাওয়া যায়নি পাউলিনা পাউলিনা লুইসি পাউলিনা লোগো জন্মস্থান প্রকাশনার তারিখ ফলাফল অনুসন্ধান করুন ফলাফল অনুসন্ধান করুন কর্ম অনুসন্ধান করুন অজানা অজানা শর্ত অজানা মান উইকিউপাত্ত উইকিমিডিয়া কমন্স উইকিমেদিস্তাস দে উরুগুয়ে উইকিপিডিয়া লিঙ্ক উইকিউক্তি লিঙ্ক উইকিসংকলন লিঙ্ক কর্ম হ্যাঁ প্রধান পাতা আফ্রিকা - অনলাইন কালচারাল সেণ্টার 